## librerias aws
from sct_tools import *
import json
import pandas as pd
from io import StringIO, BytesIO
from datetime import datetime, timezone


# formato para leer el archivo
csv_separator = ';'
csv_thousands = ','
csv_decimal = '.'
file_encoding = 'latin'

path = 'C:/PROYECTO_SEAT_AWS/JIRA/SCT_52_Integrate_Parasum/main/lambda/properties_json/'
file_name = 'parasum-schema.json'

# archivo metadato
nombre_fichero= 'C:/PROYECTO_SEAT_AWS/JIRA/SCT_52_Integrate_Parasum/main/metadatos/PARSUMMAS.txt'

# vamos a leer el fichero
f = open(nombre_fichero, 'r')
file_content = StringIO(f.read())
f.close()

# recogemos la variable file_name
file = path + file_name

# cargamos el fichero json
f = open(file, 'rb')
schema = json.load(f)
f.close()

print(get_inputfile_header(schema))
print('_______')
print(get_inputfile_column_types(schema))
print('_______')
print(get_outputfile_header(schema))
print('_______')
print(get_outputfile_ordered_header(schema))

header_columns = get_inputfile_header(schema)
column_types = get_inputfile_column_types(schema)

# Create converters for correct column parsing
# int_to_string = lambda x: str(x)
# int_to_float = lambda x: float(x)

try:
   # Check csv separator
   df = pd.read_csv(file_content,
                    sep=csv_separator,
                    header=None,
                    nrows=1,
                    encoding=file_encoding
                    )

   if df.shape[1] != len(header_columns):
       raise IOError('Incorrect csv separator')
   else:
       file_content.seek(0)

   df = pd.read_csv(file_content,
                    sep=csv_separator,
                    header=None,
                    names=header_columns,
                    encoding=file_encoding,
                    dtype=column_types,
                    #converters={
                        #'Referencia': int_to_string,
                        #'Centro': int_to_string,
                        #'Status de Material': int_to_string,
                        #'Número de lote': int_to_float,
                        #'Proveedor': int_to_string,
                        #'Capacidad Contenedor Suministor': int_to_float,
                        #'Capacidad contenedor Básico': int_to_float,
                        #'nº Contenedores Básicos de Transporte': int_to_float,
                        #'distritoProve': int_to_string,
                        #'StockSeguridad': int_to_float,
                        #'StockSeguridadMinimo': int_to_float,
                        #'Porcentaje de Asignación': int_to_float
                    #},
                    thousands = csv_thousands,
                    decimal = csv_decimal,
                    float_precision = 'high'
                    )
except Exception as e:
   #logger.error(e)
    raise e

for column_type in column_types.items():
    if column_type[1] == 'str':
        df[column_type[0]].fillna('', inplace=True)

print(df.dtypes)
#df['distritoProve'].fillna('', inplace= True)
#print(df['distritoProve'])

#print(df['Capacidad Contenedor Suministor'])

# Put Object in Cleansed File
# Get file generation datetime
json_generate_date = datetime.utcnow()
json_str_generate_date = json_generate_date.strftime(json_date_format)


# Initialize file structure
json_result = {
    'Fecha': json_str_generate_date,
    'PARASUM': [],

}

# Get output columns names-----rev------ok
new_names = get_outputfile_header(schema)
df.columns = new_names

# Reorder dataframe---rev ----OK
ordered_output_header = get_outputfile_ordered_header(schema)
df = df.reindex(columns=ordered_output_header)



df['stockSeguridad'] = df['stockSeguridad'].astype(int)
df['stockSeguridadMinimo'] = df['stockSeguridadMinimo'].astype(int)

# Get dataframe data in json format
result = df.to_json(orient='index')
result = json.loads(result)

# Fill file PARASUM
for i in result.items():
    json_result['PARASUM'].append(i[1])

# Dumps the result in JSON format local
json_result = json.dumps(json_result)
data = BytesIO(json_result.encode('utf-8'))
f=open('C:/PROYECTO_SEAT_AWS/JIRA/SCT_52_Integrate_Parasum/target/json_test.json','w')
f.write(json_result)
f.close()

